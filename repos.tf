provider "gitlab" {
  alias = "main"
}

resource "gitlab_project" "project1" {
  provider = gitlab
  name        = "project1"
  description = "1"

  visibility_level = "public"
}

resource "gitlab_project" "project2" {
  provider = gitlab
  name            = "project2"
  description = "2"

  visibility_level = "public"
}

resource "gitlab_project" "project3" {
  provider = gitlab
  name            = "project3"
  description = ""

  visibility_level = "public"
}
