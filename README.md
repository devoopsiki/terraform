# DevoOps Assignment #6
| group name | id        | fullname                        |
| ---------- | --------- | ------------------------------- |
| DevoOps    | 22B031617 | Кудайбергенов Амиржан Айдарович |
| DevoOps    | 20B030732 | Бақтыбаев Райымбек Еркешұлы     |
| DevoOps    | 21B040364 | Мурзабек Санжар Ұланұлы         |
| DevoOps    | 21B030999 | Тамирлан Нурбулатулы            |

## main.tf
Initial configuration for vm and network. 
`user-config` file is used for creation of `ansible` user and adding ssh key to access machine using ssh without password.
output is used for printed assigned external and internal ip addresses.

![terraform output](./terraform_output.png)

## install_gitlab.tf
Use provisioner "remote-exec" to install gitlab directly on vm. Commands for installation is used from tutorial.
Used ssh connection with previously created `ansible` user. Instead of domain external ip address is used.
Healthcheck is implemented with curl loop. The loop continues until request succeeds. 

## repos.tf
Creation of 3 repos with gitlab_project resource. Healthcheck is printing of links of created repos.
