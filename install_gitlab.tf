resource "terraform_data" "install_gitlab" {
  depends_on = [yandex_compute_instance.vm-1]

  connection {
    type     = "ssh"
    user     = "ansible"
    private_key = file("~/.ssh/id_ed25519")
    host     = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo apt-get install -y curl openssh-server ca-certificates tzdata",
      "curl -fsSL https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash",
      "sudo GITLAB_ROOT_PASSWORD='xxXX1234' EXTERNAL_URL='http://${yandex_compute_instance.vm-1.network_interface.0.nat_ip_address}' apt-get install gitlab-ce",
      "sudo gitlab-ctl reconfigure",
      "sudo gitlab-ctl start",
      # Health check script starts here
      "echo 'Checking the health of GitLab...'",
      "until curl --output /dev/null --silent --head --fail http://${yandex_compute_instance.vm-1.network_interface.0.nat_ip_address}/health; do",
      "    printf '.'",
      "    sleep 5",
      "done",
      "echo 'GitLab is up and healthy!'"
    ]
  }
}
